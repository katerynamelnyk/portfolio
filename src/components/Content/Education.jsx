import classes from './Content.module.css';

const Education = () => {
    return (
        <div className={classes.education}>
            <p className={classes.date}>
                2019 - 2023
                <span className={classes.mini}>(expected)</span>
            </p>
            <p>University WSB Merito in Gdansk, Poland</p>
            <p>engineering degree in Computer Science</p>
        </div>
    );
}

export default Education;