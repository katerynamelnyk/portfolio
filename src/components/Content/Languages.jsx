import classes from './Content.module.css';

const data = [
    {
        type: 'English',
        description: 'B2',
    },
    {
        type: 'Polish',
        description: 'B2',
    },
    {
        type: 'Ukrainian',
        description: 'native',
    },
];

const Languages = () => {
    const languages = data.map(item => (
        <div className={classes.language}>
            <p>{item.type}</p>
            <p>{item.description}</p>
        </div>
    ));

    return (
        <div className={classes.languages} >
            {languages}
        </div>
    );
}

export default Languages;