import classes from './Content.module.css';

const data = [
    {
        name: 'HTML',
        range: 95,
    },
    {
        name: 'CSS',
        range: 95,
    },
    {
        name: 'SASS',
        range: 90,
    },
    {
        name: 'JavaScript',
        range: 90,
    },
    {
        name: 'TypeScript',
        range: 80,
    },
    {
        name: 'GIT',
        range: 80,
    },
    {
        name: 'React',
        range: 90,
    },
    {
        name: 'Redux',
        range: 80,
    },
    {
        name: 'Next.js',
        range: 70,
    },
    {
        name: 'Mongo DB',
        range: 50,
    },
];

const Skills = () => {
    const skills = data.map(item => (
        <div className={classes.skill}>
            <p>{item.name}</p>
            <div className={classes['range-wrapper']}>
                <div className={classes.range} style={{ width: `${item.range}%` }} />
            </div>
        </div>
    ));

    return (
        <div className={classes.skills}>
            {skills}
        </div>
    );
}

export default Skills;