import classes from './Content.module.css';

const data = [
    {
        date: 'MAR`22 - present',
        title: 'Junior front-end developer',
        place: 'Go Online (Poland)',
        description: `Working on a project with React.js, Redux, TypeScript. 
        Our project is the platform that simplifies design and construction of new homes.
        This is an eTendering module that connects clients to reliable contractors and
        support the entire process of building and design.
        My main tasks are creation of new components, 
        as well as development and refactoring of existing ones.`,
    },
    {
        date: 'OCT`21 - FEB`22',
        title: 'Trainee front-end developer',
        place: 'EPAM (Ukraine)',
        description: `Learned concepts of JavaScript, CSS, AJAX, 
        DOM events and React basics.`,
    },
];

const Experience = () => {
    const experinece = data.map(item => (
        <div className={classes.experience}>
            <div>
                <p className={classes.date}>{item.date}</p>
                <p className={classes.place}>{item.place}</p>
                <p className={classes.title}>{item.title}</p>
            </div>
            <div className={classes.description}>{item.description}</div>
        </div>
    ));

    return (
        <div className={classes.experiences} >
            {experinece}
        </div>
    );
}

export default Experience;