import KateAnna from './kate-anna.jpg';
import newsApp from './news-app.jpg';
import colorApp from './color-app.jpg';
import classes from './Content.module.css';

const data = [
    {
        link: 'https://kate-anna-photo.vercel.app/',
        preview: { KateAnna },
        linkName: 'kate-anna-photo.vercel.app',
        description: (
            <p className={classes.description}>
                portfolio for photographers created using
                <span> Next.js</span>, <span>MongoDB </span>
                and <span>Google Cloud Storage</span> (for storing photos)
            </p>
        ),
    },
    {
        link: 'https://news-app-km.vercel.app/',
        preview: { newsApp },
        linkName: 'news-app-km.vercel.app',
        description: (
            <p className={classes.description}>
                an application that shows the latest news depending on the category, created using
                <span> React</span>, <span> TypeScript</span>, <span>Redux </span>,
                <span> React Rooting</span> and <span>Real-Time News Data API</span> with 
                <span> Fetch API</span>
            </p>
        ),
    },
    {
        link: 'https://color-app-blue.vercel.app/',
        preview: { colorApp },
        linkName: 'color-app-blue.vercel.app',
        description: (
            <p className={classes.description}>
                an application that allows you to add colors, display them,
                filter the displayed colors and remove them, created using
                <span> React</span>, <span> TypeScript</span>,
                and <span>LocalStorage</span> to store added colors
            </p>
        ),
    },
];

const Projects = () => {
    const projects = data.map(item => (
        <div className={classes.item}>
            <a
                href={item.link}
                target="_blank"
                rel="noreferrer"
            >
                {item.linkName}
                <div className={classes['image-wrapper']}>
                    <img
                        src={item.preview.KateAnna
                            ?? item.preview.newsApp
                            ?? item.preview.colorApp}
                        alt={item.linkName}
                    />
                </div>
            </a>
            {item.description}
        </div>
    ));

    return (
        <div className={classes.projects}>
            {projects}
        </div>
    );
}

export default Projects;