import classes from './Content.module.css';

const data = [
    {
        type: 'email',
        description: 'katyamelnik777@gmail.com',
    },
    {
        type: 'phone',
        description: '+48 731 078 545',
    },
];

const Contacts = () => {
    const contacts = data.map(item => (
        <li>
            <span>{item.type}</span>
            {item.description}
        </li>
    ));

    return (
        <div className={classes.contacts}>
            <ul>{contacts}</ul>
        </div>
    );
}

export default Contacts;