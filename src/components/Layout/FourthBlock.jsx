import Item from '../BlockItem/Item';
import Projects from '../Content/Projects';

import classes from './FourthBlock.module.css';

const FourthBlock = () => {
    return (
        <div className={classes['fourth-block']}>
            <Item
                title="Projects"
                content={<Projects />}
                isWide={true}
            />
        </div>
    );
}

export default FourthBlock;