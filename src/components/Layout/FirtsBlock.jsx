import Header from '../Header/Header';
import Contacts from '../Content/Contacts';
import Item from '../BlockItem/Item';

import classes from './FirstBlock.module.css';

const FirstBlock = () => {
    return (
        <div className={classes['first-block']}>
            <Item
                title="Contacts"
                className={classes.first}
                content={<Contacts />}
            />
            <Header />
        </div>
    );
}

export default FirstBlock;