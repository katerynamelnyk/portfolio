import Item from '../BlockItem/Item';
import Education from '../Content/Education';
import Languages from '../Content/Languages';

import classes from './ThirdBlock.module.css';

const ThirdBlock = () => {
    return (
        <div className={classes['third-block']}>
            <Item
                title="Education"
                className={classes.first}
                content={<Education />}
            />
            <Item
                title="Languages"
                content={<Languages />}
            />
        </div>
    );
}

export default ThirdBlock;