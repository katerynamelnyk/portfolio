import Item from '../BlockItem/Item';
import Experience from '../Content/Experience';
import Skills from '../Content/Skills';

import classes from './SecondBlock.module.css';

const SecondBlock = () => {
    return (
        <div className={classes['second-block']}>
            <Item
                title="Experience"
                content={<Experience />}
                className={classes.first}
            />
            <Item
                title="Skills"
                content={<Skills />}
            />
        </div>
    );
}

export default SecondBlock;