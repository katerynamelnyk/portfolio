import classes from './Decoration.module.css';

const Decoration = () => {
    return (
        <div className={classes.container}>
            <div className={classes.decoration} />
        </div>
    );
}

export default Decoration;