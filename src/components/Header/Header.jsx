import classes from './Header.module.css';

const Header = () => {
    return (
        <h2 className={classes.header}>
            <div>Kateryna Melnyk,</div>
            <div>front-end developer</div>
        </h2>
    );
}

export default Header;