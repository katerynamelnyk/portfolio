import classes from './Item.module.css';

const Item = (props) => {
    const contentClasses = props?.isWide
        ? `${classes.wide} ${classes.content}`
        : classes.content;

    return (
        <div className={`${classes.container} ${props?.className}`}>
            <div className={classes['flex-wrapper']}>
                <p className={classes.title}>{props.title}</p>
                <div className={contentClasses}>
                    {props.content}
                </div>
            </div>
        </div>
    );
}

export default Item;