import portrait from './portrait.jpg';
import classes from './About.module.css';

const About = () => {
    const title = `My name is Kateryna, I'm passionate about front end development 
    and looking for a new challenge :)`;
    const aboutMeText = `I am a native Ukrainian, currently living in Poland. 
     I’m a big fun of front end technologies and constantly develop my skills.

     Main qualities that describe myself: patience in solving problems,
     attention to details, willing to create a good product.`;

    const aboutPhoto = `Also, I'm a photographer and a contributor on Unsplash.`;

    return (
        <div className={classes.container}>
            <p className={classes.text}>
                <p className={classes.title}>{title}</p>
                <p>{aboutMeText}</p>
                <p className={classes.mini}>{aboutPhoto}</p>
            </p>
            <div className={classes['image-wrapper']}>
                <img src={portrait} alt="portrait" />
            </div>
        </div>
    );
}

export default About;