import gitLab from './gitLab.png';
import linkedIn from './linkedIn.png';
import instagram from './instagram.png';
import unsplash from './unsplash.png';
import classes from './Links.module.css';

const Links = () => {
    return (
        <div className={classes.container}>
            <a
                href="https://gitlab.com/katerynamelnyk"
                target="_blank"
                rel="noreferrer"
            >
                <div className={classes['image-wrapper']}>
                    <img src={gitLab} alt="GitLab"/>
                </div>
            </a>
            <a
                href="https://www.linkedin.com/in/kateryna-melnyk-0b4b49176"
                target="_blank"
                rel="noreferrer"
            >
                <div className={classes['image-wrapper']}>
                    <img src={linkedIn} alt="LinkedIn"/>
                </div>
            </a>
            <a
                href="https://www.instagram.com/katranos/"
                target="_blank"
                rel="noreferrer"
            >
                <div className={classes['image-wrapper']}>
                    <img src={instagram} alt="Instagram"/>
                </div>
            </a>
            <a
                href="https://unsplash.com/@katranos"
                target="_blank"
                rel="noreferrer"
            >
                <div className={classes['image-wrapper']}>
                    <img src={unsplash} alt="Unsplash"/>
                </div>
            </a>
        </div>
    );
}

export default Links;