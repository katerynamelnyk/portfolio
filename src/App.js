import FirstBlock from './components/Layout/FirtsBlock';
import SecondBlock from './components/Layout/SecondBlock';
import Decoration from './components/UI/Decoration';
import ThirdBlock from './components/Layout/ThirdBlock';
import FourthBlock from './components/Layout/FourthBlock';
import About from './components/About/About';
import Links from './components/About/Links';


const App = () => {
  return (
    <>
      <Decoration />
      <FirstBlock />
      <SecondBlock />
      <ThirdBlock />
      <FourthBlock />
      <About />
      <Links />
    </>
  );
}

export default App;
